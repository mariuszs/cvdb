# CVDB Resume Database #

The CVDB Resume Database is a prototypical project that demonstrates the usage of AngularJS in combination with Spring 4.
It can be used as a starting point to develop *real* applications.

### Online Presentation ###
The following presentation points out the most interesting parts of the project:
http://www.slideshare.net/owahlen/angularjs-spring4

### Build Setup ###

In order to build the project locally you will need to install the following tools:

* gradle
* npm
* bower
* grunt
* yeoman

In the cvdb-client folder you may need to execute the following commands to fetch dependent JavaScript files:

* npm install
* bower install

The project is build and/or started by calling one of the following commands in the cvdb-server folder:

* gradle build
* gradle war
* gradle jar
* gradle bootRun