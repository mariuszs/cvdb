'use strict';

describe('Directive: myTable', function () {

    // load the directive's module
    beforeEach(module('cvdbClientApp'));

    var parentScope;
    var scope;

    beforeEach(inject(function ($templateCache, $rootScope, $compile) {
        $templateCache.put('/views/directives/mytable.html', '<div>dummy div</div>');
        parentScope = $rootScope.$new();
        parentScope.sort = 'sortCol';
        parentScope.order = 'asc';
        parentScope.onRowClicked = function() {};
        spyOn(parentScope, 'onRowClicked');
        var element = angular.element('<my-table sort="sort" order="order" on-row-clicked="onRowClicked(row)"></my-table>');
        element = $compile(element)(parentScope);
        parentScope.$apply();
        scope = element.isolateScope();
    }));

    it('should return false for isSortedBy', inject(function () {
        expect(scope.isSortedBy('otherCol')).toBe(false);
    }));

    it('should return true for isSortedBy', inject(function () {
        expect(scope.isSortedBy('sortCol')).toBe(true);
    }));

    it('should return false for isOrder', inject(function () {
        expect(scope.isOrder('desc')).toBe(false);
    }));

    it('should return true for isOrder', inject(function () {
        expect(scope.isOrder('asc')).toBe(true);
    }));

    it('should toggle sorting onHeaderClicked', inject(function () {
        scope.onHeaderClicked({name: 'sortCol', sortable: true});
        expect(scope.sort).toBe('sortCol');
        expect(scope.order).toBe('desc');
    }));

    it('should sort by new column onHeaderClicked', inject(function () {
        scope.onHeaderClicked({name: 'newCol', sortable: true});
        expect(scope.sort).toBe('newCol');
        expect(scope.order).toBe('asc');
    }));

    it('should call onRowClicked', inject(function() {
        scope._onRowClicked('foo');
        expect(parentScope.onRowClicked).toHaveBeenCalledWith('foo');
    }));

});
