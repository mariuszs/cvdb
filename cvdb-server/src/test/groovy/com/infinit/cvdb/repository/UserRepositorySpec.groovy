package com.infinit.cvdb.repository

import com.infinit.cvdb.domain.User
import com.infinit.cvdb.repository.UserRepository
import com.infinit.cvdb.test.TestSpec
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Unroll

/**
 * Created by owahlen on 31.12.13.
 */
class UserRepositorySpec extends TestSpec {

	@Autowired
	UserRepository userRepository

	@Unroll
	def "findByName should find User '#username'"(String username) {
		when:
		User user = userRepository.findByUsername(username)

		then:
		user
		user.username == username

		where:
		username << ['admin', 'manager', 'user', 'test']
	}

}
